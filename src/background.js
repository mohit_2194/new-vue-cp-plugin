global.browser = require('webextension-polyfill')
/*Polyfill for Mozilla Firefox*/
import store from './store'

// alert(`Hello ${store.getters.foo}!`)


//Fire function for tab url changes

chrome.tabs.query({currentWindow: true}, function(tabs){
    console.log(tabs)
    chrome.tabs.onUpdated.addListener(function(tabId,changeInfo,tab){
        store.dispatch('updateCurrentUrl', tab.url)
        // console.log(`Hello ${store.state.currenturl}`)
    });

    chrome.tabs.onActivated.addListener(function(activeInfo){
        // console.log(`Hello ${store.state.currenturl}`)
        tabs.forEach(function(tab){
            if(activeInfo.tabId === tab.id){
                store.dispatch('updateCurrentUrl', tab.url)
            }
        })
    })

    chrome.tabs.onCreated.addListener(function(tab){
        store.dispatch('updateCurrentUrl', tab.url)
        // console.log(`Hello ${store.state.currenturl}`)
    })
})

store.watch(function(state,getter){
    console.log(state.currenturl)
})

