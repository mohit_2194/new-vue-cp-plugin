// import * as types from './mutation-types'

export default {

  updateCurrentUrl(state, data) {
    state.currenturl = data
  },

  updateSavedWebsites(state,data) {
    state.savedwebsites = data
  }
}
