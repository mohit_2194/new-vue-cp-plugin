import Vue from 'vue'
import Vuex from 'vuex'
import VuexChromePlugin from './../vuex-plugin/index'

import * as getters from './getters'
import mutations from './mutations'
import * as actions from './actions'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currenturl: 'about:blank',
    savedwebsites: [],
  },
  getters,
  mutations,
  actions,
  plugins: [VuexChromePlugin()],
})
