// import * as types from './mutation-types'

// export const updateFoo = ({commit}, payload) => {
//   commit('updateFoo', payload)
// }

export const updateCurrentUrl = ({commit}, payload) => {
  commit('updateCurrentUrl', payload)
}

export const updateSavedWebsites = ({commit}, payload) => {
  commit('updateSavedWebsites', payload)
}
