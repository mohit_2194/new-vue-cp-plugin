import PageIndex from './pages/Index'
import Websites from './pages/Pages'

export default [
  {
    path: '/',
    component: PageIndex
  },
  {
    path:'/websites',
    component: Websites
  }
]
