import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import VueResource from 'vue-resource';

Vue.use(VueRouter)
Vue.use(VueResource);

export default new VueRouter({
  routes,

})
